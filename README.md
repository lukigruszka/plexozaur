# Plexozaur
### Opis
Robot klasy linefollower mający za zadanie podążać za linią. Sterowany jest za pomocą uniwersalnego pilota na podczerwień. Zbudowany jest na bazie 8-bitowego mikrokontrolera firmy Atmel.
### Zdjęcia
![Zdjęcie 1](/img/ukos.jpg "Widok z ukosa")
![Zdjęcie 2](/img/tyl.jpg "Widok z tyłu")
### Zastrzeżenia
* Powolne silniki
* Duża masa
* Wrażliwość na zakłocenia (możliwa reakcja na obce sygnały IR)