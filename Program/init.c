#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "io_cfg.h"
#include "init.h"

void init() {
	/* inicjuj LED-y testowe */
	LED_INI;
	LED_TOGGLE_BIT_INI;

	SREG |= (1<<7); //umożliwienie wszystkich przerwań
	
	piny_setup();
	czujnik_setup();
	pwm_setup();
	dd_rc5_ini();
	signal(); //pierwsze odpalenie w celu zapalenia diody trybu
	
    sei();
}

void piny_setup(){
	//------------------------------------------------------------------
	DDRA	= 0b10000000; //wyłączy jedynie ósmy czujnik
	
	DDRB	|= (1<<NIEB) | (1<<ZIEL) | (1<<CZERW) | (1<<ZOLT) | (1<<RC_LED);
	
	DDRC	= 0xFF;
	
	DDRD	&= ~((1<<RC_BUTTON) | (1<<BUTTON));
	DDRD	|= (1<<EN_11) | (1<<EN_12) | (1<<EN_21)
			| (1<<PD4)  //PWM 1 kanał
			| (1<<PD5); //PWM 2 kanał
	
	//------------------------------------------------------------------
	PORTD	|=  (1<<RC_BUTTON);
	PORTD	&= ~((1<<PD4) | (1<<PD5));
	
	_delay_ms(10);
}

void czujnik_setup(){
	
	/** ADC SETUP **/
	ADCSRA |= ((1<<ADEN) // włączenie przetwornika ADC
	| (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0)); //ustawia preskaler na 128,
	ADCSRA &= ~((1<<ADIE) | (1<<ADATE)); // Deaktywacja ADC Interrupt Enable i auto-trigger
	
	/* Napięcie odniesienie wew. 2,56V */
	ADMUX |= ((1<<REFS1) | (1<<REFS0));  
	
	/* External Interrupt Request 0 */
	SFIOR &= ~((1<<ADTS2) | (1<<ADTS0) | (1<<ADTS1));
	//SFIOR |= (1<<ADTS1); 
	
	
	/** TIMER2 SETUP **/
	/* Timer2 Overflow Interupt */
	TIMSK |= (1<<OCIE2);
	
	/* Timer2 Prescaler 1024 */
	TCCR2 |= (1<<CS22) | (1<<CS21) | (1<<CS20);
	
	/* Clear Timer on Compare match (CTC) mode */
	TCCR2 |= (1<<WGM21);
	TCCR2 &= ~(1<<WGM20);
	
	/* Timer2 liczy do 156, bo 16MHz/1024/156 = 100Hz */
	OCR2 = 156;
}

void pwm_setup() {
	/* Timer1 non-inverting mode */
	TCCR1A |= (1<<COM1A1) | (1<<COM1B1); 
	TCCR1A &= ~((1<<COM1A0) | (1<<COM1B0)); 
	
	/* Timer1 Fast PWM, 10-bit */
	TCCR1A |= (1<<WGM10) | (1<<WGM11);
	TCCR1B |= (1<<WGM12);
	TCCR1B &= ~(1<<WGM13);
	
	/* Timer1 no-prescaling - 16MHz / 2^10 = 15,6kHz */
	TCCR1B |= (1<<CS10);
	TCCR1B &= ~((1<<CS11) | (1<<CS12));
	
	/* Przykładowe wartości (0-1023) */
	left_motor = 1023;
	right_motor = 1023;

	/*OCR1AH = 0xFF;
	OCR1AL = 0xFF;
	
	OCR1BH = 0xFF;
	OCR1BL = 0xFF;*/
}
