#ifndef IO_CFG_H
#define IO_CFG_H

#define F_CPU 16000000L

/* LED testowy do pokazywania odebranych danych */
#define LED_PORT	PORTB
#define LED_DDR 	DDRB
#define LED_PIN 	PB2
#define LED_INI 	LED_DDR	 |=  (1<<LED_PIN)
#define LED_ON		LED_PORT |=  (1<<LED_PIN)
#define LED_OFF		LED_PORT &= ~(1<<LED_PIN)

/* LED testowy do pokazywania stanu bitu toggle */
#define LED_TOGGLE_BIT_PORT	PORTB
#define LED_TOGGLE_BIT_DDR 	DDRB
#define LED_TOGGLE_BIT_PIN 	PB1
#define LED_TOGGLE_BIT_INI 	LED_TOGGLE_BIT_DDR 	|= 	(1<<LED_TOGGLE_BIT_PIN)
#define LED_TOGGLE_BIT_ON	LED_TOGGLE_BIT_PORT |=  (1<<LED_TOGGLE_BIT_PIN)
#define LED_TOGGLE_BIT_OFF	LED_TOGGLE_BIT_PORT &= ~(1<<LED_TOGGLE_BIT_PIN)

/* Diodki, czujniki, inne duperele */
#define RC_LED		PB0 //Czy włączony RC-5
#define CZERW		PB1
#define ZIEL		PB2
#define ZOLT		PB3
#define NIEB		PB4
#define BUTTON		PD0
#define RC_BUTTON	PD1

/* Diody czujników */
#define LED0	PC1
#define LED1	PC2
#define LED2 	PC3
#define LED3	PC4
#define LED4	PC5
#define LED5	PC6
#define LED6	PC7

/* Mostek H (silniki, kierunki) */
#define EN_11 PD3
#define EN_12 PD6
#define EN_21 PD7
#define EN_22 PC0
#define EN_11_ON	PORTD |= (1<<EN_11)
#define EN_11_OFF	PORTD &= ~(1<<EN_11)
#define EN_12_ON	PORTD |= (1<<EN_12)
#define EN_12_OFF	PORTD &= ~(1<<EN_12)
#define EN_22_ON	PORTD |= (1<<EN_21)
#define EN_22_OFF	PORTD &= ~(1<<EN_21)
#define EN_21_ON	PORTC |= (1<<EN_22)
#define EN_21_OFF	PORTC &= ~(1<<EN_22)
#define left_motor 	OCR1B
#define right_motor OCR1A

#endif
