#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "io_cfg.h"
#include "linefollower.h"
#include "dd_rc5.h"

/* Tryby programu */
int program = 27; //domyślne tryb jazdy Y

/* Regulator P */
int p_bazowa_silnika = 500;
int p_wskKp = 200;
int p_waga = 0;
int p_l = 0, p_p = 0;

/* Regulator PD */
int pd_bazowa_silnika = 800; //do 700
int pd_wskKp = 28, pd_wskKd = 400; //W miarę działa wskKp = 120, wskKd = 400
int pd_tab_wag[7] = {15, 10, 4, 0, -4, -10, -15}; //W miarę działa {4, 2, 1, 0, -1, -2, -4}
int pd_waga = 0, pd_tmp_waga = 0, pd_roznica_wagi = 0, pd_tmp_roznica_wagi = 0;
int pd_timer = 0, pd_timer_max = 300;
float pd_linia = 0.25;
int pd_l = 0, pd_p = 0;

/* Zmienna od guzika */
uint8_t key_lock = 0;

/* Wartosci czujników i wartośc domyślna */
int czujnik[7] = {0};
int domyslna = 200;

/* Zmienne do zczytywania wartości ADC */
uint8_t theLowADC = 0;
uint16_t wholeADC = 0;

/* Zmienne globalne z odebranymi danymi */
volatile unsigned char dd_rc5_dane_odebrane = 0;
volatile unsigned char dd_rc5_status = 0;
volatile int czy_pomiar = 0, czy_jazda = 0;

void PD(){ //wagi odwrotnie, niż w P (ujemne z lewej)
	pd_tmp_waga = pd_waga;
	pd_waga = 0;
	
	for(int i = 0; i < 7; i++)
		if(czujnik[i] == 1)
			pd_waga += pd_tab_wag[i];
			
	pd_roznica_wagi = pd_waga - pd_tmp_waga; //róznica > 0 --> reaguj w prawo
	
	if(pd_roznica_wagi != 0){
		pd_timer = pd_timer_max;
		pd_tmp_roznica_wagi = pd_roznica_wagi;
	}
			
	if(pd_waga != 0 || czujnik[3] == 1){ //nie włączy się i trzyma stan, jeśli stracił linię
		pd_l = pd_bazowa_silnika + pd_waga*pd_wskKp;
		pd_p = pd_bazowa_silnika - pd_waga*pd_wskKp;
	}
	
	if(pd_timer != 0){
		if(pd_waga != 0){
			pd_l += pd_tmp_roznica_wagi*pd_wskKd;
			pd_p -= pd_tmp_roznica_wagi*pd_wskKd;
			pd_timer--;	
		}
		else {
             if(pd_tmp_roznica_wagi > 0) { //mocniej prawym
             pd_l -= 2*pd_linia*pd_tmp_roznica_wagi*pd_wskKd;
			 pd_p += -0.2*pd_linia*pd_tmp_roznica_wagi*pd_wskKd;
             }
             else if(pd_tmp_roznica_wagi < 0) {
             pd_l -= -0.2*pd_linia*pd_tmp_roznica_wagi*pd_wskKd;
			 pd_p += 2*pd_linia*pd_tmp_roznica_wagi*pd_wskKd;
             }
			pd_timer = 0;	
		}
	}
	PWM(pd_l, pd_p);
}

void P(){
	p_waga = 0;
	for(int i = 0; i < 7; i++) 
		if(czujnik[i] == 1)
			p_waga += i-3;
	
	if(p_waga != 0 || czujnik[3] == 1){ 
		p_l = p_bazowa_silnika - (p_waga*p_wskKp);
		p_p = p_bazowa_silnika + (p_waga*p_wskKp);
	}
	
	PWM(p_l, p_p);
}

void zrob_pomiary(){
	for(int i=0; i<7; i++){
		ADMUX &= 0b11100000;			// zerujemy bity MUX odpowiedzialne za wybór kana
		ADMUX |= i;						// wybieramy kanał przetwornika
		ADCSRA |= (1<<ADSC);			// Załącz pomiar
		while(ADCSRA & (1<<ADSC)) {};	// czekamy aż skoñczy się pomiar
		
		theLowADC = ADCL;
		wholeADC = ADCH<<8 | theLowADC;
		
		if(wholeADC > domyslna)
			czujnik[i] = 1;
		else
			czujnik[i] = 0;
	}
}

void pokaz_czujniki(){
	if(czujnik[0] == 1)
		PORTC |= (1<<LED0);
	else
		PORTC &= ~(1<<LED0);

	if(czujnik[1] == 1)
		PORTC |= (1<<LED1);
	else
		PORTC &= ~(1<<LED1);
		
	if(czujnik[2] == 1)
		PORTC |= (1<<LED2);
	else
		PORTC &= ~(1<<LED2);
		
	if(czujnik[3] == 1)
		PORTC |= (1<<LED3);
	else
		PORTC &= ~(1<<LED3);
		
	if(czujnik[4] == 1)
		PORTC |= (1<<LED4);
	else
		PORTC &= ~(1<<LED4);
		
	if(czujnik[5] == 1)
		PORTC |= (1<<LED5);
	else
		PORTC &= ~(1<<LED5);
		
	if(czujnik[6] == 1)
		PORTC |= (1<<LED6);
	else
		PORTC &= ~(1<<LED6);
}

int zrob_pomiary_pojedynczego(int ktory) {
	if(ktory > 6 || ktory < 0)
		return 0;
	
	ADMUX &= 0b11100000;
	ADMUX |= ktory;	
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC)) {};
	theLowADC = ADCL;
	wholeADC = ADCH<<8 | theLowADC;
	
	return wholeADC;
}

void signal(){
	if(dd_rc5_dane_odebrane == 25 || dd_rc5_dane_odebrane == 23 || dd_rc5_dane_odebrane == 27 || dd_rc5_dane_odebrane == 36){
		program = dd_rc5_dane_odebrane;
		PWM(0,0);
	}
				
	//wymigaj_sygnal();
	
	dd_rc5_status &= ~DD_RC5_STATUS_DANE_GOTOWE_DO_ODCZYTU;
	DD_RC5_WLACZ_DEKODOWANIE;
}

void wymigaj_sygnal(){
	unsigned char f;

	//sprawdzamy bit toggle	zapalaj¹c przypisan¹ mu diodê
	//gdy bit ten jest jedynk¹ i gasz¹c, gdy jest zerem
	if(dd_rc5_status & 0b100000)	LED_TOGGLE_BIT_ON;
	else LED_TOGGLE_BIT_OFF;

	//opóźnienie w celu lepszej widoczności zmian diod LED
	PORTB |= (1<<NIEB);
	_delay_ms(300);
	


	// mrugnij diod¹ danych tyle razy jaki kod klawisza odebrano 
	 //dd_rc5_dane_odebrane = dd_rc5_status & 0b11111;
	   for(f=0; f<dd_rc5_dane_odebrane; f++){
			LED_ON;
			_delay_ms(50);
			LED_OFF;
			_delay_ms(300);
	}

	//Poniewa¿ odczytaliśmy odebrane dane zgaś flagê gotowości
	//danych do odczytu, by umo¿liwiæ odebranie nastêpnych danych
	dd_rc5_status &= ~DD_RC5_STATUS_DANE_GOTOWE_DO_ODCZYTU;
	DD_RC5_WLACZ_DEKODOWANIE;
}

void guzik(){
	if( !key_lock && !(PIND & (1<<RC_BUTTON) ) ) {
		key_lock=1;
		PORTB ^= (1<<NIEB);
	}
	else if( key_lock && (PIND & (1<<RC_BUTTON) ) )  
		if( !++key_lock ) {
			PORTB ^= (1<<NIEB);
	}
}

void PWM(int lewy, int prawy) { 
    if(lewy >= 0) { 
        if(lewy>1023) 
            lewy = 1023; 
        EN_12_ON; 
        EN_11_OFF; 
    } 
    else { 
        if(lewy<-1023) 
            lewy = -1023; 
        EN_12_OFF; 
        EN_11_ON;    
    } 
    
    if(prawy >= 0) { 
        if(prawy>1023) 
            prawy = 1023; 
        EN_22_ON; 
        EN_21_OFF; 
    } 
    else { 
        if(prawy<-1023) 
            prawy = -1023; 
        EN_22_OFF; 
        EN_21_ON;    
    } 
    
    left_motor = abs(lewy); //abs(x) --> zwraza wart. bezwzględną liczby x
    right_motor = abs(prawy); 
}

void kalibracja(){
	PORTB |= (1<<CZERW);
	PORTB &= ~((1<<ZIEL) | (1<<ZOLT) | (1<<NIEB));
	
	zrob_pomiary_pojedynczego(dd_rc5_dane_odebrane-1);
	
	for(int i = 1; i <= 7; i++)
		if(wholeADC > i*1023/8)
			PORTC |= (1<<i);
		else
			PORTC &= ~(1<<i);
}

void zdalny(){
	PORTB |= (1<<ZIEL);
	PORTB &= ~((1<<CZERW) | (1<<ZOLT) | (1<<NIEB));
	
	switch(dd_rc5_dane_odebrane){
		case 42:
			PWM(500, 500);
			break;
		case 10:
			PWM(200, 50);
			break;
		case 11:
			PWM(50, 200);
			break;
		case 46:
			PWM(-200, -200);
			break;
		default:
			PWM(0,0);
			break;
	}
}

void jazda(){
	PORTB |= (1<<ZOLT);
	PORTB &= ~((1<<CZERW) | (1<<ZIEL) | (1<<NIEB));
	
	if(czy_pomiar == 1) {
		zrob_pomiary();
		pokaz_czujniki();
		czy_pomiar = 0;
	}
	
	if(dd_rc5_dane_odebrane == 1 || dd_rc5_dane_odebrane == 43)
		PD();
	else if(dd_rc5_dane_odebrane == 2 || dd_rc5_dane_odebrane == 14 || dd_rc5_dane_odebrane == 41)
		PWM(0,0);
}

void wyjebungo(){
	PORTB |= (1<<NIEB);
	PORTB &= ~((1<<CZERW) | (1<<ZOLT) | (1<<ZIEL));

	if(czy_pomiar == 1) {
		zrob_pomiary();
		pokaz_czujniki();
		czy_pomiar = 0;
	}
	
	PD();
}
