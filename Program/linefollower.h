#ifndef LINEFOLLOWER_H
#define LINEFOLLOWER_H

void kalibracja(), zdalny(), jazda(), wyjebungo();
void P();
void PD();
void signal();
void wymigaj_sygnal();
void guzik();
void zrob_pomiary();
void pokaz_czujniki();
int zrob_pomiary_pojedynczego(int ktory);
void PWM(int lewy, int prawy);

extern uint8_t key_lock, theLowADC;
extern uint16_t wholeADC;
extern int program;
extern int czujnik[7], domyslna;
extern int p_bazowa_silnika, p_wspKp, p_waga, p_l, p_p;
extern int pd_bazowa_silnika, pd_wskKp, pd_wskKd, pd_tab_wag[], pd_waga, pd_tmp_waga, pd_roznica_wagi, pd_tmp_roznica_wagi, pd_timer, pd_timer_max, pd_l, pd_p;
extern float pd_linia;

extern volatile unsigned char dd_rc5_dane_odebrane, dd_rc5_status;
extern volatile int czy_pomiar, czy_jazda;

/* Ropziska sygnałów:
 * 25 	Red (kalibracja)
 * 
 * 23 	Green (tryb zdalny)
 * 42 	Do przodu 	| |>
 * 10 	W prawo 		>>
 * 11 	W lewo 		<<
 * 46 	Do tyłu 		<| |
 * 
 * 27 	Yellow (tryb jazdy)
 * 46 	Start			|>
 * 1 	Start
 * 14 	Stop 			||
 * 41	Stop			[]
 * 2 	Stop
 * 
 * 36 Blue (wyjebungo)
 */

#endif
