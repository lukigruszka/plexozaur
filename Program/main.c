/*

Test biblioteki RC-5 na ATmega8

Schemat testowy:
 - trzy diody LED anodami do pinów PB0, PD6 i PD7, a katodami przez rezystory
   220Ohm do GND (masa).
 - wyjcie czujnika podczerwieni do pinu INT0.

Plik:		RC5_main.c
Kompilator: AVR-GCC 4.3.3

Data: 	2013.07.30 
Autor: 	Dondu
www:	http://mikrokontrolery.blogspot.com/2011/03/RC5-IR-podczerwien-odbior-danych-przyklad-AVR-ATmega8.html

Schemat testowy dostêpny w linku powy¿ej.

*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "io_cfg.h"
#include "init.h"
#include "linefollower.h"
#include "dd_rc5.h"

int main(void){
	
	init();
	
    while(1){
		
		//guzik();

		if(program != 36 && (dd_rc5_status & DD_RC5_STATUS_DANE_GOTOWE_DO_ODCZYTU)){ signal(); } //sprawdź, czy dane są gotowe do odczytu 
		
		switch(program){
			case 25:
				kalibracja();
				break;
			case 23:
				zdalny();
				break;
			case 27:
				jazda();
				break;
			case 36:
				wyjebungo();
				break;
		}
	}
}

ISR(TIMER2_COMP_vect) {
	czy_pomiar = 1;
}
